// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;


import {SimpleStorage} from "./SimpleStorage.sol";


contract AddFiveStorage is SimpleStorage {
    // To inherit from another contract, we need to add "is" followed by the contract it's inheriting from.
    // To change (override) functionality of an inherited method, there are 2 keywords we must use:
    // 1. virtual: Must be added to the original method, to declare that it is in fact overridable
    // 2. override: Must be added in the method that overrides the original one

    function store(uint256 _newNumber) public override {
        myFavoriteNumber = _newNumber + 5;
    }
    
}
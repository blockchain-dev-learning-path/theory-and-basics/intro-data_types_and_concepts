// SPDX-License-Identifier: MIT
pragma solidity 0.8.19; // stating our Solidity version

contract SimpleStorage {
    // basic Types: boolean, uint, int, address, bytes
    // uint: unsigned integer

    bool hasFavoriteNumber = true;
    
    uint public favoriteNumber; // Initiates variable as 0 for ints.

    uint256 favoriteNumber256 = 88;
    // uint, as default is uint256

    int intFavNumber = -88;

    string favoriteNumberText = "88"; // Strings are actually bytes objects

    address myAddress = 0x4BC9679F56015a9af29486146552B29A95a74989;
    bytes32 favoriteBytes32 = "cat"; 

    uint256[] listOfFavNums; // this is an array of uint256

    // In solidity we can create our own types using the type "struct"
    struct Person {
        string name;
        uint256 favNum;
    }

    // dynamic array:
    Person[] public listOfPeople;

    // static array:
    Person[3] public staticListOfPeople;

    mapping(string => uint256) public nameToFavoriteNumber; 

    function store(uint256 _favoriteNumber) public {
        favoriteNumber = _favoriteNumber;
    }


    function retrieve() public view returns(uint256) {
        // This function doesn't change the state of the blockchain, != store(). Only retrieves something.
        return favoriteNumber;
    }

    function retrievePure() public pure returns(uint256) {
        return 7;
    }
    
    function addPerson(string memory _name, uint256 _favNumber) public {
        listOfPeople.push(
            Person({
                favNum: _favNumber,
                name: _name
            })
        );

        nameToFavoriteNumber[_name] = _favNumber;
    }

}
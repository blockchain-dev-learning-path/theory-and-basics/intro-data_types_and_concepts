// SPDX-License-Identifier: MIT
pragma solidity 0.8.19; // stating our Solidity version


// Basic Types: boolean, uint, int, address, bytes

// - uint: unsigned integer

// Variable Declarations:

contract BasicDataTypes {

    bool hasFavoriteNumber = true;

    uint public favoriteNumber; // Initiates variable as 0 for ints.
    // "public" is a visibility specifier. More on that later.

    uint256 favoriteNumber256 = 88;
    // uint, as default is uint256

    int intFavNumber = -88;

    string favoriteNumberText = "88"; // Strings are actually bytes objects

    address myAddress = 0x4BC9679F56015a9af29486146552B29A95a74989;
    bytes32 favoriteBytes32 = "cat"; 

}

contract ComplexTypes {
    uint256[] listOfFavNums; // this is an array of uint256
    // Arrays can be static or dynamic:
    // static:
    uint256[3] staticArr;
    // dynamic:
    uint256[] dynamicArr;


    // In solidity we can create our own types using the type "struct"
    struct Person {
        uint256 favNum;
        string name;
    }

    Person public myFriend = Person({
        favNum: 7, 
        name: "Pat"
        }
    );

    mapping(string => uint256) public nameToFavoriteNumber; 
}

contract VisibilitySpecifiers {

    // Function visibility specifiers:
    // - public: visible externally and internally (creates a getter function for storage/state variables)
    // - private: only visible in current contract
    // - external: only visible externally (only for functions) - i.e. can only be message-called (via this.func)
    // - internal: only visible internally

}


contract Modifiers {
    // Different Keywords 
    // These notates functions that don't have to run or you don't have to send a transaction for you to call them:

    // - view: Reads a state from the blockchain, for example a variable like "favoriteNumber".
    // If it has the "view" keyword, it WON'T ALLOW any operation that updates the state of the blockchain, like "favoriteNumber = favoriteNumber +1".

    // - pure: Doesn't read from a state of the blockchain. i.e.: Can't return favoriteNumber, but could return, for example: 7.
}



contract StoringData {
    // EVM can access and store information in six places:
    /*
        1. Stack
        2. Memory: temporary variable. Exists for a single call. Variable can be modified
        3. Storage: permanent variable. Can be modified. Exist outside functions calls.
        4. Calldata: temporary variable. Exists for a single call. Variable CAN'T be modified
        5. Code
        6. Logs
    */

    // Data location can only be specified for array, struct, or mapping types. (STRING is an array of bytes)
}
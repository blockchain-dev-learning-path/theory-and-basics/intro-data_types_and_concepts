// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

// Get funds from users
// Withdraw funds
// Set a minimum funding value in USD

import {PriceConverter} from "./PriceConverter.sol";

contract FundMe {

    using PriceConverter for uint256; // This is adding all the functions from our library PriceConverter to all types uint256 variables. 
    // e.g.: msg.value.getConversionRate()

    uint256 public constant MINIMUM_USD = 5e18; // Declarar constantes consume menos gas que las variables.

    address[] public funders;
    mapping(address funder => uint256 amountFunded) public addressToAmountFunded;

    address public immutable owner;

    constructor() {
        owner = msg.sender;
    }

    function fund() public payable {
        // Allow users to send $
        // Have a minimum $ sent - use "require" function
        // 1. How to send ETH to this contract?
        // 1.a. Make the function "payable"

        // msg is a reserved keyword in Solidity, and has the attr. "value" which represents the value being sent in the transaction
        require(msg.value.getConversionRate() >= MINIMUM_USD, "Not enough ETH sent."); // 1e18 = 1 ETH = 1000000000000000000 = 1 * 10 ** 18
        // The "require" function is a checker. If condition not met, it reverts the whole transaction to its original state.
        // A failed transaction DOES SPEND GAS!

        funders.push(msg.sender);
        addressToAmountFunded[msg.sender] = addressToAmountFunded[msg.sender] + msg.value;
    }



    function withdraw() public onlyOwner {
        // Reset on withdrawal
        for(uint256 funderIndex = 0; funderIndex < funders.length; funderIndex++) {
            address funder = funders[funderIndex];
            addressToAmountFunded[funder] = 0;
        }

        // Reset the array
        funders = new address[](0);

        // Withdraw the funds
        // Different methods:

        // Transfer. payable(msg.sender) = payable address
        payable(msg.sender).transfer(address(this).balance);
        // Send
        bool sendSuccess = payable(msg.sender).send(address(this).balance);
        require(sendSuccess, "Send failed.");
        // Call
        (bool callSuccess, ) = payable(msg.sender).call{value: address(this).balance}("");
        require(callSuccess, "Call failed.");

    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Sender is not owner!");
        _;
    }

}